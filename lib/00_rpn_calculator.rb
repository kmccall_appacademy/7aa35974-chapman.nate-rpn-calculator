class RPNCalculator

  def initialize
    @stack = []
  end

  # push places another operand onto the stack
  def push(num)
    @stack << num
  end

  # will add the last two integers of the stack and push it onto
  # the stack
  def plus
    perform_operation(:+)
  end

  def value
    @stack.last
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    string.split.map do |char|
      is_operation?(char) ? char.to_sym : Integer(char)
    end
  end

  private

  def is_operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end

  def perform_operation(operator)
    raise "calculator is empty" if @stack.size < 2

    case operator
    when :+
      @stack << @stack.pop + @stack.pop
    when :-
      @stack << @stack.pop * - 1 + @stack.pop
    when :*
      @stack << @stack.pop * @stack.pop
    when :/
      @stack << 1.0 / @stack.pop.to_f * @stack.pop.to_f
    end
  end
end
